//
//  Product.swift
//  ShopPractica1
//
//  Created by Ivan Cozar Laguna on 23/04/2019.
//  Copyright © 2019 Ivan Cozar Laguna. All rights reserved.
//

import Foundation
// Las estructuras tienen constructor por defecto, las clases no
struct Product {
    
    let name: String
    let price: Int
    let units: Int
}
