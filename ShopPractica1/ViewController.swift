//
//  ViewController.swift
//  ShopPractica1
//
//  Created by Ivan Cozar Laguna on 23/04/2019.
//  Copyright © 2019 Ivan Cozar Laguna. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    // crear
    var products = [Product]()
    // sin crear
    // var products: [Product]()
    
    
    override func viewDidLoad() {
        
        products.append(Product(name: "Patatas", price: 60, units: 0))
        products.append(Product(name: "Soja", price: 60, units: 0))
        products.append(Product(name: "Cafe", price: 60, units: 0))
        products.append(Product(name: "Colacao", price: 60, units: 0))
        products.append(Product(name: "Hamburguesa", price: 60, units: 0))
        
        super.viewDidLoad()
    }


}



extension ViewController: UITableViewDataSource{
    // Numero de elementos por seccion, sino se dice nada solo tiene una seccion
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCellIdentifier", for: indexPath)
        
        let product = products[indexPath.row]
        
        cell.textLabel?.text = "\(product.name)  - \(product.price)€"
        
        return cell
    }
    
    
}

